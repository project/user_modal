OVERVIEW
========
User modal module allows opening the Register/ Login/ Reset password menu items
as tabs. Since the tabs are shown via JS and not AJAX, this leads to a better 
user experience, as there is hardly and time waiting for the selected tab to 
load.

The goal of user modal is to allow easy and fast registration or login, thus it 
is recommended to set the account settings via admin/config/people/accounts
- "Who can register accounts" set to Visitors
- Uncheck "Require e-mail verification when a visitor creates an account."
The above settings will insure that a registered user will immediatly be logged 
in

DEVELOPERS
==========
