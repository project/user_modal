(function ($) {

/**
 * Hide and show more info.
 */
Drupal.behaviors.userModal = {
  attach: function (context) {
  
   $('#tabs', context)
     // Turn div to tabs, and select the tab as was send by the server.
     .tabs({"selected": Drupal.settings.userModal.userSelectedTab})
     // Bind click on tabs, so we can send back to server which tab is 
     // selected.
     .bind('tabsselect', function(event, ui) {
       // Set the selection of the tab.
       $('.form-item-user-action select').val(ui.index);
     });
  }
};
  


})(jQuery);


